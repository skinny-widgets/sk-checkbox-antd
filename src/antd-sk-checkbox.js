import { SkCheckboxImpl }  from '../../sk-checkbox/src/impl/sk-checkbox-impl.js';

export class AntdSkCheckbox extends SkCheckboxImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'checkbox';
    }

    get checked() {
        return this.input.hasAttribute('checked');
    }

    set checked(checked) {
        if (checked) {
            this.input.checked = 'checked';
            this.input.setAttribute('checked', 'checked');
            this.comp.el.querySelector('.ant-checkbox').classList.add('ant-checkbox-checked');
        } else {
            this.input.checked = false;
            this.input.removeAttribute('checked');
            this.comp.el.querySelector('.ant-checkbox').classList.remove('ant-checkbox-checked');
        }
    }
    // sk-checkbox element's context
    onClick(event) {
        this.checked = ! this.checked;
        if (this.checked) {
            //this.comp.el.querySelector('input').checked = 'checked';
            this.inact = true;
            this.comp.setAttribute('checked', 'checked');
            this.inact = false;
            //this.comp.el.querySelector('.ant-checkbox').classList.add('ant-checkbox-checked');
        } else {
            //this.comp.el.querySelector('input').checked = false;
            this.inact = true;
            this.comp.removeAttribute('checked');
            //this.comp.el.querySelector('.ant-checkbox').classList.remove('ant-checkbox-checked');
            this.inact = false;
        }
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.comp.inputEl.setCustomValidity(result);
            this.showInvalid();
        }
    }

    bindEvents() {
        if (this.checked) {
            this.input.setAttribute('checked', this.checked);
        }
        this.comp.onclick = function(event) {
            this.onClick(event);
        }.bind(this);
/*        this.comp.el.querySelector('input').onchange = function(event) {
            if (this.input.getAttribute('checked')) {
                this.check();
            } else {
                this.uncheck();
            }
        }.bind(this);*/
    }

    check() {
        super.check();
        this.comp.el.querySelector('.ant-checkbox').classList.add('ant-checkbox-checked');
    }

    uncheck() {
        super.uncheck();
        this.comp.el.querySelector('.ant-checkbox').classList.remove('ant-checkbox-checked');
    }

    disable() {
        super.disable();
        this.comp.el.querySelector('.ant-checkbox').classList.add('ant-checkbox-disabled');
    }

    enable() {
        super.enable();
        this.comp.el.querySelector('.ant-checkbox').classList.remove('ant-checkbox-disabled');
    }

    unbindEvents() {
        super.unbindEvents();
        this.comp.removeEventListener('click', this.clickHandle);
    }

    showInvalid() {
        super.showInvalid();
        this.comp.el.querySelector('.ant-checkbox').classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.comp.el.querySelector('.ant-checkbox').classList.remove('form-invalid');
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }

}
